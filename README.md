## 環境配置
關閉 Swap
```bash
sudo swapoff -a && sudo sysctl -w vm.swappiness=0
```
```bash
sudo cat <<EOF | tee /etc/sysctl.d/k8s.conf  \
net.ipv4.ip_forward = 1  \
net.bridge.bridge-nf-call-ip6tables = 1  \
net.bridge.bridge-nf-call-iptables = 1  \
EOF

sudo sysctl -p /etc/sysctl.d/k8s.conf
```
### Install Docker Engine - Community
[Docker Official Doc](https://docs.docker.com/install/linux/docker-ce/debian/)
```bash
sudo apt-get update && sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
```
```bash
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```
```bash
sudo apt-key fingerprint 0EBFCD88
```
```bash
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
```
> [依照不同系統配置不同參數](https://docs.docker.com/install/linux/docker-ce/debian/)

#### Install Docker Engine - Community
```bash
sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io
apt-cache madison docker-ce
```

## Install kubelet kubeadm kubectl
[K8S Official DOC](https://kubernetes.io/zh/docs/setup/production-environment/tools/kubeadm/)
#### Before you begin
-   One or more machines running one of:
    -   Ubuntu 16.04+
    -   Debian 9+
    -   CentOS 7
    -   Red Hat Enterprise Linux (RHEL) 7
    -   Fedora 25+
    -   HypriotOS v1.0.1+
    -   Container Linux (tested with 1800.6.0)
-   ==2 GB== or more of RAM per machine (any less will leave little room for your apps)
-   ==2 CPUs== or more
-   Full network connectivity between all machines in the cluster (public or private network is fine)
-   Unique hostname, MAC address, and product_uuid for every node. See  [here](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/#verify-the-mac-address-and-product-uuid-are-unique-for-every-node)  for more details.
-   Certain ports are open on your machines. See  [here](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/#check-required-ports)  for more details.
-   Swap disabled. You  **MUST**  disable swap in order for the kubelet to work properly
### Install kubelet kubeadm kubectl
```bash
sudo apt-get update && sudo apt-get install -y sudo apt-transport-https curl

curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -

sudo cat <<EOF >/etc/apt/sources.list.d/kubernetes.list /
deb https://apt.kubernetes.io/ kubernetes-xenial main /
EOF
sudo apt-get update && sudo apt-get install -y kubelet kubeadm kubectl
sudo apt-mark hold kubelet kubeadm kubectl
```
## Kubernetes Multi Node
在master-node執行
```bash
sudo kubeadm init --service-cidr 10.96.0.0/12 --pod-network-cidr 10.244.0.0/16 --apiserver-advertise-address 0.0.0.0
```
> 預設k8s api port為 6443
> 
執行完畢後會取得下面訊息 
>Your Kubernetes control-plane has initialized successfully!
To start using your cluster, you need to run the following as a regular user:
  `mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config`
You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/
Then you can join any number of worker nodes by running the following on each as root:
`kubeadm join 172.31.27.231:6443 --token 4mwgks.dc2090y3k5m28u86 \
    --discovery-token-ca-cert-hash sha256:eee40066d3b238f66c4b3ee32e1bc63eb75a601360df8d2a68e5740468437458 `

執行
```bash
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```
**CNI**
```bash
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
```
**ingress-controller**
```bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-0.32.0/deploy/static/provider/baremetal/deploy.yaml
```
___
### Work Node
在Work node上執行剛剛在master node 取得的join 指令完成Join 
```bash
sudo kubeadm join 172.31.27.231:6443 --token 4mwgks.dc2090y3k5m28u86 \
    --discovery-token-ca-cert-hash sha256:eee40066d3b238f66c4b3ee32e1bc63eb75a601360df8d2a68e5740468437458
```

==執行`kubectl get node` 確認node 的status 為 Ready==
